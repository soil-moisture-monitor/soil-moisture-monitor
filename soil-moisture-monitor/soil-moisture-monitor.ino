/************************************************************
Soil_Moisture_Monitor.ino
Andrew Gagnon
************************************************************/

//////////////////////
// Library Includes //
//////////////////////

// SoftwareSerial is required (even you don't intend on
// using it).
#include <SoftwareSerial.h> 
#include <SparkFunESP8266WiFi.h>
#include <math.h>

// Attach the serial display's RX line to digital pin 2
// pin 2 = TX, pin 3 = RX (unused)
SoftwareSerial mySerial(3,2); 

// Soil moisture thresholdss
int thresholdUp = 400;
int thresholdDown = 250;

// We are setting up the pin A0 on the redboard to be our sensor
// pin input:s
int soilPin = A0;
//Variable for Soil moisture Power
int soilPower = 7;

//////////////////////////////
// WiFi Network Definitions //
//////////////////////////////

const char mySSID[] = "";
const char myPSK[] = "";

// Setup functions
void setup() 
{
  Serial.begin(9600);
  serialTrigger(F("Press any key to begin."));

  // initializeESP8266() verifies communication with the WiFi sshield, and sets it up.
  initializeESP8266();

  // connectESP8266() connects to the defined WiFi network.
  connectESP8266();

  // displayConnectInfo prints the Shield's local IP
  // and the network it's connected to.
  //displayConnectInfo();

  //Set D7 as an OUTPUT
  pinMode(soilPower, OUTPUT);
  //Set to LOW so no power is flowing through the sensor
  digitalWrite(soilPower, LOW);

}

void loop() 
{
 
  String DisplayWords;

  int sensorValue;
  sensorValue = readSoil();

  // move cursor to beginning of first line on LCD:
  mySerial.write(254); 
  mySerial.write(128);

  // clear display:
  mySerial.write("                "); 
  mySerial.write("                ");

  // move cursor to beginning of first line of the LCD screen:
  mySerial.write(254); 
  mySerial.write(128);

  //Write what we want to desplay on the screen:
  mySerial.write("Water Level: ");
  mySerial.print(sensorValue); //Using .print instead of .write for values

  if (sensorValue <= thresholdDown){
    // move cursor to beginning of second line on LCD:
    mySerial.write(254); 
    mySerial.write(192);

    DisplayWords = "Dry, Water it!";
    mySerial.print(DisplayWords);
  } else if (sensorValue >= thresholdUp){
    // move cursor to beginning of second line on LCD:
    mySerial.write(254); 
    mySerial.write(192);

    DisplayWords = "Wet, Leave it!";
    mySerial.print(DisplayWords);

    } else {
    // move cursor to beginning of second line on LCD:
    mySerial.write(254); 
    mySerial.write(192);

    mySerial.print(DisplayWords);
  }

  int sensorValueByteLength = ceil(log10(sensorValue)+1);
  int contentLength = 40+sensorValueByteLength;

  const String httpRequestKafka = "POST /topics/smm HTTP/1.1\n"
                           "Host: ec2-18-223-107-196.us-east-2.compute.amazonaws.com:8082\n"
                           "Connection: close\n"
                           "Content-Type: application/vnd.kafka.json.v2+json\n"
                           "Content-Length: " + contentLength + "\n"
                           "\n"
                           "{\"records\":[{\"value\":{\"smm_reading\":" + sensorValue + "}}]}";

  ESP8266Client client;

  int retVal = client.connect("ec2-18-223-107-196.us-east-2.compute.amazonaws.com", 8082);
  if (retVal <= 0)
  {
    Serial.println(F("Failed to connect to server.123"));
    return;
  }

  client.print(httpRequestKafka);

  while (client.available())
    Serial.write(client.read()); // read() gets the FIFO char
  
  if (client.connected())
    client.stop(); // stop() closes a TCP connection.
}

void initializeESP8266()
{
  // esp8266.begin() verifies that the ESP8266 is operational
  // and sets it up for the rest of the sketch.
  // It returns either true or false -- indicating whether
  // communication was successul or not.
  // true
  int test = esp8266.begin();
  if (test != true)
  {
    Serial.println(F("Error talking to ESP8266."));
    errorLoop(test);
  }
  Serial.println(F("ESP8266 Shield Present"));
}

void connectESP8266()
{
  // The ESP8266 can be set to one of three modes:
  //  1 - ESP8266_MODE_STA - Station only
  //  2 - ESP8266_MODE_AP - Access point only
  //  3 - ESP8266_MODE_STAAP - Station/AP combo
  // Use esp8266.getMode() to check which mode it's in:
  int retVal = esp8266.getMode();
  if (retVal != ESP8266_MODE_STA)
  { // If it's not in station mode.
    // Use esp8266.setMode([mode]) to set it to a specified
    // mode.
    retVal = esp8266.setMode(ESP8266_MODE_STA);
    if (retVal < 0)
    {
      Serial.println(F("Error setting mode."));
      errorLoop(retVal);
    }
  }
  Serial.println(F("Mode set to station"));

  // esp8266.status() indicates the ESP8266's WiFi connect
  // status.
  // A return value of 1 indicates the device is already
  // connected. 0 indicates disconnected. (Negative values
  // equate to communication errors.)
  retVal = esp8266.status();
  if (retVal <= 0)
  {
    Serial.print(F("Connecting to "));
    Serial.println(mySSID);
    // esp8266.connect([ssid], [psk]) connects the ESP8266
    // to a network.
    // On success the connect function returns a value >0
    // On fail, the function will either return:
    //  -1: TIMEOUT - The library has a set 30s timeout
    //  -3: FAIL - Couldn't connect to network.
    retVal = esp8266.connect(mySSID, myPSK);
    if (retVal < 0)
    {
      Serial.println(F("Error connecting"));
      errorLoop(retVal);
    }
  }
}

void displayConnectInfo()
{
  char connectedSSID[24];
  memset(connectedSSID, 0, 24);
  // esp8266.getAP() can be used to check which AP the
  // ESP8266 is connected to. It returns an error code.
  // The connected AP is returned by reference as a parameter.
  int retVal = esp8266.getAP(connectedSSID);
  if (retVal > 0)
  {
    Serial.print(F("Connected to: "));
    Serial.println(connectedSSID);
  }

  // esp8266.localIP returns an IPAddress variable with the
  // ESP8266's current local IP address.
  IPAddress myIP = esp8266.localIP();
  Serial.print(F("My IP: ")); Serial.println(myIP);
}


// errorLoop prints an error code, then loops forever.
void errorLoop(int error)
{
  Serial.print(F("Error: ")); Serial.println(error);
  Serial.println(F("Looping forever."));
  for (;;)
    ;
}

// serialTrigger prints a message, then waits for something
// to come in from the serial port.
void serialTrigger(String message)
{
  Serial.println();
  Serial.println(message);
  Serial.println();
  while (!Serial.available())
    ;
  while (Serial.available())
    Serial.read();
}

//This is a function used to get the soil moisture content
int readSoil()
{
    digitalWrite(soilPower, HIGH);//turn D7 "On"
    delay(10);//wait 10 milliseconds 
    int val = analogRead(soilPin);//Read the SIG value form sensor 
    digitalWrite(soilPower, LOW);//turn D7 "Off"
    return val;//send current moisture value
}
