# Soil-Moisture-Monitor

Monitor soil moisture using the Sparksfun soil moister sensor

# Goals

- [x] Set up soil moisture monitor with LCD display
- [x] Integrate wifi shield 
  - [x] Solder header pins to wifi shield
  - [x] Verify connectivity between redboard and wifi shield
- [x] Set up Kafka server
  - [ ] Modify script to produce soil readings to Google Firestore
- [ ] Apply regression analysis to collected data


![](https://gitlab.com/soil-moisture-monitor/env/raw/master/Images/20190208-DSCF2745.jpg)